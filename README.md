# README #

### What is this repository for? ###
Make cUrl requests with rotating proxy and output to screen or file. For each request, you POST with a new proxy address.
Supports Http & Https

### Setup ###
+ $ npm install phantomjs
+ $ Clone repository to folder

### Run ###
+ $ ./singurl.sh "https://google.com"