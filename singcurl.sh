#!/bin/bash
url=$1

function rotatePrx() {
> prxtmp.txt
phantomjs printSource.js "https://proxy-list.org/english/search.php?search=&country=any&type=any&port=any&ssl=any" | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}:[0-9]\{1,8\}' | shuf -n 1 >> prxtmp.txt
export prxnum=$(head -n 1 prxtmp.txt)
#echo $prxnum
}
rotatePrx

function getReq() {
> ttxt.txt
> ckie
curl -k -L -b ckie -c ckie -v -m 30 --proxy $prxnum "$url" -s -o /dev/null -w "%{http_code}" >> ttxt.txt
}

getReq

function changeProx() {
resp=$(head -n 1 ttxt.txt)
if [[ "$resp" -eq "0" ]];
then
echo "Trying Again.."
rotatePrx
getReq
else
echo "Proxy Connection Success"
fi

}

changeProx